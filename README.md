## RUN MySQL Source

`docker run -p 3306:3306 --name my-mysql -e MYSQL_ROOT_PASSWORD=admin -v $HOME/mysql-data:/var/lib/mysql -d mysql`


### pre-requesties

1- JDBC connector (you don't need to download it for this repo it's already included)

2- Mysql connector (you don't need to download it for this repo it's already included)


write your mysql properties file

this is my naming for the file you don't need to use same as it `etc/kafka/thiqah-mysql.properties`

```
name=test-source-mysql-jdbc-event-notifier
connector.class=io.confluent.connect.jdbc.JdbcSourceConnector
tasks.max=1
connection.user=root
connection.password=admin
connection.url=jdbc:mysql://127.0.0.1:3306/DBname
# tables=[tableName]
mode=timestamp
validate.non.null=false
timestamp.column.name=lastUpdated
# incrementing.column.name=id
topic.prefix=my-prefix-
```


* Note: you must create a timestamp field in your DB so the connector can know it's new event. that's why i used `timestamp.column.name=lastUpdated` because the timestamp column in my DB was `lastUpdated`


## Run zookeeper server

`bin/zookeeper-server-start etc/kafka/zookeeper.properties`


## Run kafka server

`bin/kafka-server-start etc/kafka/server.properties`


## Run Schema Registry

`bin/schema-registry-start etc/schema-registry/schema-registry.properties`


bin/connect-standalone etc/kafka/connect-standalone.properties etc/kafka/thiqah-sqlserver-cdc.properties


bin/kafka-console-consumer --topic thiqah.dbo.people --bootstrap-server localhost:9092




### Resources:

https://www.tutorialkart.com/apache-kafka/kafka-connector-mysql-jdbc/

https://stackoverflow.com/questions/53538802/confluent-error-failed-to-run-query-for-table-timestampincrementingtablequerier

https://stackoverflow.com/questions/53745747/confluent-jdbc-connector-cannot-make-incremental-queries-using-timestamp-columns

https://docs.confluent.io/kafka-connect-jdbc/current/source-connector/source_config_options.html

https://dev.mysql.com/downloads/connector/j/

https://www.youtube.com/watch?v=gPvwvkCVSnY

https://www.confluent.io/hub/confluentinc/kafka-connect-jdbc

